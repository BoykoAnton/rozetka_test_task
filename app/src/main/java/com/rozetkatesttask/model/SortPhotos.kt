package com.rozetkatesttask.model

enum class SortPhotos {
    popular, lates, oldest
}