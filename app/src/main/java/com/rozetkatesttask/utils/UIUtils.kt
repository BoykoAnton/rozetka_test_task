package com.rozetkatesttask.utils

import android.app.ProgressDialog
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

object UIUtils {
    fun showProgressDialog(progress: ProgressDialog?, cancelable: Boolean) {
        if (progress == null || progress.isShowing) return

        progress.setCancelable(cancelable)
        progress.show()
    }

    fun hideProgressDialog(progress: ProgressDialog?) {
        if (progress != null && progress.isShowing) {
            progress.cancel()
        }
    }

    fun hideKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}