package com.rozetkatesttask.api.models.listphoto

data class ProfileImage (
	val small : String?,
	val medium : String?,
	val large : String?
)