package com.rozetkatesttask.api.models.searchphoto

data class SearchPhotoResponse (
	val total : Int?,
	val total_pages : Int?,
	val results : List<Results?>?
)