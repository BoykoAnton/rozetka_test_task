package com.rozetkatesttask.api.models.searchphoto

data class Links (
	val self : String?,
	val html : String?,
	val download : String?
)