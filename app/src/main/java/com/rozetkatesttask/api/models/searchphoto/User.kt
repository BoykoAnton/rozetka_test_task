package com.rozetkatesttask.api.models.searchphoto

data class User (
	val id : String?,
	val username : String?,
	val name : String?,
	val first_name : String?,
	val last_name : String?,
	val instagram_username : String?,
	val twitter_username : String?,
	val portfolio_url : String?,
	val profile_image : ProfileImage,
	val links : Links
)