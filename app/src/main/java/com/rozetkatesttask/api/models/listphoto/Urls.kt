package com.rozetkatesttask.api.models.listphoto

data class Urls (
	val raw : String?,
	val full : String?,
	val regular : String?,
	val small : String?,
	val thumb : String?
)