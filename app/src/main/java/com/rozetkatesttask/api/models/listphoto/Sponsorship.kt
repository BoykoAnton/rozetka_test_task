package com.rozetkatesttask.api.models.listphoto

data class Sponsorship (
	val impression_urls : List<String?>?,
	val tagline : String?,
	val tagline_url : String?,
	val sponsor : Sponsor?
)