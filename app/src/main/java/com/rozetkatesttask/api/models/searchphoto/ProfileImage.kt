package com.rozetkatesttask.api.models.searchphoto

data class ProfileImage (
	val small : String?,
	val medium : String?,
	val large : String?
)