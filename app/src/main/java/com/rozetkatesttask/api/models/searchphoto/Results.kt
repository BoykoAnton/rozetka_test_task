package com.rozetkatesttask.api.models.searchphoto

import com.rozetkatesttask.api.models.listphoto.Urls

data class Results (
	val id : String?,
	val created_at : String?,
	val width : Int?,
	val height : Int?,
	val color : String?,
	val blur_hash : String?,
	val likes : Int?,
	val liked_by_user : Boolean?,
	val description : String?,
	val user : User?,
	val current_user_collections : List<String?>?,
	val urls : Urls?,
	val links : Links?
)