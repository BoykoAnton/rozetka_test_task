package com.rozetkatesttask.api.models.listphoto

data class Links (
	val self : String?,
	val html : String?,
	val photos : String?,
	val likes : String?,
	val portfolio : String?,
	val following : String?,
	val followers : String?
)