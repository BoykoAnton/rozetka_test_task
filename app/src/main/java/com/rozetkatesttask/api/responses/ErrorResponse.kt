package com.rozetkatesttask.api.responses

data class ErrorResponse (val message: String?, val id: Int?)