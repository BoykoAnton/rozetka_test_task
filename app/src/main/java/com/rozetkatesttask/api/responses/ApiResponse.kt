package com.rozetkatesttask.api.responses

data class ApiResponse<out T : Any>(val data: T?, val error: ErrorResponse?)