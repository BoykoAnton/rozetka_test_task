package com.rozetkatesttask.api.retrofit

import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.searchphoto.SearchPhotoResponse
import com.rozetkatesttask.api.responses.ApiResponse

class ApiRepository(private val api: Api) : BaseRepository() {
    suspend fun getListPhotos(
        page: Int,
        perPage: Int,
        orderBy: String
    ): ApiResponse<List<PhotosResponse>> {
        val response = safeApiCall(
            call = {
                api.getListPhotos(
                    orderBy = orderBy,
                    page = page,
                    per_page = perPage
                ).await()
            }
        )
        return response
    }

    suspend fun searchPhotos(
        page: Int,
        perPage: Int,
        query: String
    ): ApiResponse<SearchPhotoResponse> {
        val response = safeApiCall(
            call = {
                api.searchPhotos(
                    query = query,
                    page = page,
                    per_page = perPage
                ).await()
            }
        )
        return response
    }
}