package com.rozetkatesttask.api.retrofit

import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.searchphoto.SearchPhotoResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface Api {
    @GET("photos")
    fun getListPhotos(
        @Query("order_by") orderBy: String,
        @Query("page") page: Int,
        @Query("per_page") per_page: Int
        ): Deferred<Response<List<PhotosResponse>>>

    @GET("search/photos")
    fun searchPhotos(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") per_page: Int
    ): Deferred<Response<SearchPhotoResponse>>
}