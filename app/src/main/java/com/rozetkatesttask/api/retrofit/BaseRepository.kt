package com.rozetkatesttask.api.retrofit

import com.google.gson.JsonParser
import com.rozetkatesttask.api.responses.ApiResponse
import com.rozetkatesttask.api.responses.ErrorResponse
import retrofit2.Response
import android.util.Log

sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val exception: ErrorResponse) : Result<Nothing>()
}

open class BaseRepository {
    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): ApiResponse<T> {
        val result: Result<T> = safeApiResult(call)
        var data: T? = null
        var errorResponse: ErrorResponse? = null
        when (result) {
            is Result.Success ->
                data = result.data
            is Result.Error -> {
                errorResponse = result.exception
                Log.d("TEST", result.exception.toString())
            }
        }

        return ApiResponse(data, errorResponse)

    }

    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): Result<T> {
        val response = try {
            call.invoke()
        } catch (e: Exception) {
            null
        }

        if (response == null) {
            val e = ErrorResponse("Произошла ошибка подключения, попробуйте пожалуйста позже", 1001)
            return Result.Error(e)
        }

        if (response.isSuccessful) return Result.Success(response.body()!!)

        Log.d("TEST", response.code().toString())
        val errorJsonString = response.errorBody()?.string()
        val message: String = try {
            JsonParser().parse(errorJsonString)
                .asJsonObject["message"]
                .asString
        } catch (e: Exception) {
            response.message()
        }
        val e = ErrorResponse(message, response.code())
        return Result.Error(e)
    }
}