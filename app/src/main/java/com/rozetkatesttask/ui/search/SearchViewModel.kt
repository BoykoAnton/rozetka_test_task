package com.rozetkatesttask.ui.search

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.listphoto.Urls
import com.rozetkatesttask.api.models.searchphoto.SearchPhotoResponse
import com.rozetkatesttask.api.responses.ApiResponse
import com.rozetkatesttask.mvvmcore.BaseViewModel
import kotlinx.coroutines.launch

class SearchViewModel(application: Application) : BaseViewModel(application) {
    val repository = SearchRepository()

    var ldSuccess: MutableLiveData<List<Urls>> = MutableLiveData()
    var ldSuccessAddData: MutableLiveData<List<Urls>> = MutableLiveData()
    var ldError: MutableLiveData<Boolean> = MutableLiveData()

    var photoData: ArrayList<Urls> = ArrayList()

    fun resetSearch() {
        photoData.clear()
        ldSuccess.postValue(photoData)
    }

    fun searchPhotosNext(page: Int, perPage: Int, query: String?) {
        scope.launch {
            val response = query?.let {
                repository.searchPhotos(
                    query = it,
                    page = page,
                    perPage = perPage
                )
            }
            response?.let { processResponse(it, false) }
        }
    }

    fun searchPhotos(page: Int, perPage: Int, query: String?) {
        scope.launch {
            val response = query?.let {
                repository.searchPhotos(
                    query = it,
                    page = page,
                    perPage = perPage
                )
            }
            response?.let { processResponse(it, true) }
        }
    }

    private fun processResponse(response: ApiResponse<SearchPhotoResponse>, newData: Boolean) {
        if (response.error != null) {
            ldError.postValue(true)
            if (!response.error.message.isNullOrBlank()) {
                showToast(response.error.message)
            }
            return
        }
        response.data?.let {
            photoData.clear()
            for (photo in it.results!!){
                photo?.urls?.let { it1 -> photoData.add(it1) }
            }
            if (newData) {
                ldSuccess.postValue(photoData)
            } else {
                ldSuccessAddData.postValue(photoData)
            }
        }
    }
}