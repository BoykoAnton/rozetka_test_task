package com.rozetkatesttask.ui.search

import com.rozetkatesttask.api.models.searchphoto.SearchPhotoResponse
import com.rozetkatesttask.api.responses.ApiResponse
import com.rozetkatesttask.api.retrofit.ApiRepository
import com.rozetkatesttask.api.retrofit.ApiService

class SearchRepository {
    suspend fun searchPhotos(
        page: Int,
        perPage: Int,
        query: String
    ): ApiResponse<SearchPhotoResponse> {
        return ApiRepository(ApiService.api).searchPhotos(
            page = page,
            perPage = perPage,
            query = query
        )
    }
}