package com.rozetkatesttask.ui.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import com.rozetkatesttask.R
import com.rozetkatesttask.databinding.ActivitySearchBinding
import com.rozetkatesttask.mvvmcore.BaseActivity
import com.rozetkatesttask.ui.main.adapters.PaginationScrollListener
import com.rozetkatesttask.ui.main.adapters.RvTransformAdapter
import kotlinx.coroutines.*

private const val ACTIVITY_LAYOUT_ID = R.layout.activity_search

class SearchActivity : BaseActivity<SearchViewModel, ActivitySearchBinding>() {
    companion object {
        fun newIntent(context: Context?) =
            Intent(
                context,
                SearchActivity::class.java
            )
    }

    override val layoutId: Int = ACTIVITY_LAYOUT_ID

    override fun createViewMode() = provideViewModel { SearchViewModel(it) }

    override fun setDataToBinding() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        setListeners()
        setObservers()
    }

    private var layoutManager: GridLayoutManager? = null
    private var adapter: RvTransformAdapter? = null
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var currPage: Int = 1
    var query: String? = null

    private fun initView() {
        layoutManager = GridLayoutManager(this, 3)
        binding.rvPhotos.layoutManager = layoutManager
        adapter = RvTransformAdapter(layoutManager)
        binding.rvPhotos.adapter = adapter
        layoutManager?.spanCount = 1
    }

    private fun setListeners() {
        binding.btnGoBack.setOnClickListener {
            onBackPressed()
        }

        layoutManager?.let {
            binding.rvPhotos.addOnScrollListener(object : PaginationScrollListener(it) {
                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }

                override fun loadMoreItems() {
                    isLoading = true
                    getMoreItems(query)
                }
            })
        }

        binding.searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Main)
                private var searchJob: Job? = null

                override fun onQueryTextSubmit(query: String?): Boolean {
                    viewModel.searchPhotos(1, 30, query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    searchJob?.cancel()
                    searchJob = coroutineScope.launch {
                        newText?.let {
                            delay(500)
                            if (it.isEmpty()) {
                                viewModel.resetSearch()
                                currPage = 1
                            } else {
                                viewModel.searchPhotos(1, 30, it)
                                query = it
                            }
                        }
                    }
                    return false
                }
            }
        )
    }

    private fun setObservers() {
        viewModel.ldSuccess.observe(this, {
            adapter?.clearItems()
            adapter?.setItems(it)
            adapter?.notifyDataSetChanged()
        })

        viewModel.ldSuccessAddData.observe(this, {
            adapter?.addData(it)
            adapter?.notifyDataSetChanged()
            isLoading = false
        })
    }

    fun getMoreItems(query: String?) {
        currPage++
        viewModel.searchPhotosNext(currPage, 30, query)
    }
}
