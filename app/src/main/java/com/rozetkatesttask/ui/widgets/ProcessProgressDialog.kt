package com.rozetkatesttask.ui.widgets

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import com.rozetkatesttask.R

private const val LAYOUT_DIALOG = R.layout.dialog_progress

class ProcessProgressDialog(context: Context?) : ProgressDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(LAYOUT_DIALOG)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}