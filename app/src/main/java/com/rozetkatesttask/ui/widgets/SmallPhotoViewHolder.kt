package com.rozetkatesttask.ui.widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.rozetkatesttask.R
import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.listphoto.Urls
import com.squareup.picasso.Picasso

class SmallPhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), IBindPhoto {
    constructor(parent: ViewGroup)
            : this(
        LayoutInflater.from(parent.context).inflate(R.layout.item_photo_small, parent, false)
    )

    override fun bind(photo: Urls) {
        val img = itemView.findViewById<ImageView>(R.id.img)
        Picasso.with(itemView.context)
            .load(photo.small)
            .placeholder(R.drawable.ic_image_placeholder)
            .error(R.drawable.ic_no_image)
            .centerCrop()
            .fit()
            .into(img);
    }
}