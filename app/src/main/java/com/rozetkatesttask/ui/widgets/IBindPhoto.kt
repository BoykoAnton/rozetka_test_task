package com.rozetkatesttask.ui.widgets

import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.listphoto.Urls

interface IBindPhoto {
    fun bind(photoData: Urls)
}