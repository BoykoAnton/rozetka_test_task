package com.rozetkatesttask.ui.main.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.listphoto.Urls
import com.rozetkatesttask.ui.fullphoto.FullPhotoActivity
import com.rozetkatesttask.ui.widgets.IBindPhoto
import com.rozetkatesttask.ui.widgets.RegularPhotoViewHolder
import com.rozetkatesttask.ui.widgets.SmallPhotoViewHolder

class RvTransformAdapter(
    private val layoutManager: GridLayoutManager? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val photoDataList: ArrayList<Urls> = ArrayList()

    enum class ViewType {
        SMALL,
        REGULAR
    }

    fun setItems(photoData: List<Urls>) {
        photoDataList.addAll(photoData)
        notifyDataSetChanged()
    }

    fun clearItems() {
        photoDataList.clear()
        notifyDataSetChanged()
    }

    fun addData(listItems: List<Urls>) {
        var size = this.photoDataList.size
        this.photoDataList.addAll(listItems)
        var sizeNew = this.photoDataList.size
        notifyItemRangeChanged(size, sizeNew)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ViewType.REGULAR.ordinal -> RegularPhotoViewHolder(parent)
            else -> SmallPhotoViewHolder(parent)
        }
    }

    override fun onBindViewHolder(viewH: RecyclerView.ViewHolder, position: Int) {
        val viewHolder: IBindPhoto = viewH as IBindPhoto
        viewHolder.bind(photoDataList[position])
        viewH.itemView.setOnClickListener {
            viewH.itemView.context.startActivity(FullPhotoActivity.newIntent(viewH.itemView.context, photoDataList.get(position)))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (layoutManager?.spanCount == 1) ViewType.REGULAR.ordinal
        else ViewType.SMALL.ordinal
    }

    override fun getItemCount(): Int {
        return photoDataList.size
    }
}