package com.rozetkatesttask.ui.main

import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.responses.ApiResponse
import com.rozetkatesttask.api.retrofit.ApiRepository
import com.rozetkatesttask.api.retrofit.ApiService

class MainRepository {
    suspend fun getListPhotos(
        page: Int,
        perPage: Int,
        orderBy: String
    ): ApiResponse<List<PhotosResponse>> {
        return ApiRepository(ApiService.api).getListPhotos(
            page = page,
            perPage = perPage,
            orderBy = orderBy
        )
    }
}