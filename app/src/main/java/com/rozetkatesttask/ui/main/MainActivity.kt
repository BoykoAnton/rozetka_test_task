package com.rozetkatesttask.ui.main

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.Window
import android.widget.Button
import android.widget.RadioButton
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.rozetkatesttask.R
import com.rozetkatesttask.databinding.ActivityMainBinding
import com.rozetkatesttask.model.SortPhotos
import com.rozetkatesttask.mvvmcore.BaseActivity
import com.rozetkatesttask.ui.main.adapters.PaginationScrollListener
import com.rozetkatesttask.ui.main.adapters.RvTransformAdapter
import com.rozetkatesttask.ui.search.SearchActivity

private const val ACTIVITY_LAYOUT_ID = R.layout.activity_main

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>(),
    BottomNavigationView.OnNavigationItemSelectedListener {
    companion object {
        fun newIntent(context: Context?) =
            Intent(
                context,
                MainActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    }

    override val layoutId: Int = ACTIVITY_LAYOUT_ID

    override fun createViewMode() = provideViewModel { MainViewModel(it) }

    override fun setDataToBinding() {}

    private var layoutManager: GridLayoutManager? = null
    private var adapter: RvTransformAdapter? = null
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var currPage: Int = 1

    var currentSort: SortPhotos = SortPhotos.popular

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        setListeners()
        setObservers()
        viewModel.getListPhotosWithClear(1, 30, currentSort.toString())
    }

    private fun initView() {
        layoutManager = GridLayoutManager(this, 3)
        binding.rvPhotos.layoutManager = layoutManager
        adapter = RvTransformAdapter(layoutManager)
        binding.rvPhotos.adapter = adapter
        layoutManager?.spanCount = 1
    }

    private fun setListeners() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(this)

        layoutManager?.let {
            binding.rvPhotos.addOnScrollListener(object : PaginationScrollListener(it) {
                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }

                override fun loadMoreItems() {
                    isLoading = true
                    getMoreItems()
                }
            })
        }

        binding.btnSearch.setOnClickListener {
            startActivity(SearchActivity.newIntent(this))
        }
    }

    private fun setObservers() {
        viewModel.ldSuccess.observe(this, {
            adapter?.setItems(it)
            adapter?.notifyDataSetChanged()
        })

        viewModel.ldSuccessAddData.observe(this, {
            adapter?.addData(it)
            adapter?.notifyDataSetChanged()
            isLoading = false
        })
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_sort -> {
                showDialog()
            }

            R.id.menu_change_view -> {
                if (layoutManager?.spanCount == 1) {
                    layoutManager?.spanCount = 3
                    item.title = resources.getString(R.string.menu_item_list)
                    item.icon = getDrawable(R.drawable.ic_list)
                } else {
                    layoutManager?.spanCount = 1
                    item.title = resources.getString(R.string.menu_item_grid)
                    item.icon = getDrawable(R.drawable.ic_grid)
                }
                adapter?.notifyItemRangeChanged(0, adapter?.itemCount ?: 0)
            }
        }
        return true
    }

    fun getMoreItems() {
        currPage++
        viewModel.getListPhotos(currPage, 30, currentSort.toString())
    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_sort_photos)
        val applyButton = dialog.findViewById(R.id.apply_button) as Button
        val sortByPopularity = dialog.findViewById(R.id.rb_sort_popular) as RadioButton
        val sortByLatest = dialog.findViewById(R.id.rb_sort_latest) as RadioButton
        val sortByOldest = dialog.findViewById(R.id.rb_sort_oldest) as RadioButton

        when (currentSort) {
            SortPhotos.popular -> sortByPopularity.isChecked = true
            SortPhotos.lates -> sortByLatest.isChecked = true
            SortPhotos.oldest -> sortByOldest.isChecked = true
        }

        applyButton.setOnClickListener {
            if (sortByLatest.isChecked) {
                currentSort = SortPhotos.lates
            }
            if (sortByOldest.isChecked) {
                currentSort = SortPhotos.oldest
            }
            if (sortByPopularity.isChecked) {
                currentSort = SortPhotos.popular
            }
            adapter?.clearItems()
            adapter?.notifyDataSetChanged()
            viewModel.getListPhotosWithClear(1, 30, currentSort.toString())

            dialog.dismiss()
        }
        dialog.show()
    }
}
