package com.rozetkatesttask.ui.main

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.listphoto.Urls
import com.rozetkatesttask.api.responses.ApiResponse
import com.rozetkatesttask.mvvmcore.BaseViewModel
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : BaseViewModel(application) {
    val repository = MainRepository()

    var ldSuccess: MutableLiveData<List<Urls>> = MutableLiveData()
    var ldSuccessAddData: MutableLiveData<List<Urls>> = MutableLiveData()
    var ldError: MutableLiveData<Boolean> = MutableLiveData()

    var photoData: ArrayList<Urls> = ArrayList()

    fun getListPhotos(
        page: Int,
        perPage: Int,
        orderBy: String
    ) {
        scope.launch {
            val response = repository.getListPhotos(
                orderBy = orderBy,
                page = page,
                perPage = perPage
            )
            processResponse(response, false)
        }
    }

    fun getListPhotosWithClear(
        page: Int,
        perPage: Int,
        orderBy: String
    ) {
        scope.launch {
            val response = repository.getListPhotos(
                orderBy = orderBy,
                page = page,
                perPage = perPage
            )
            processResponse(response, true)
        }
    }

    private fun processResponse(response: ApiResponse<List<PhotosResponse>>, newData: Boolean) {
        if (response.error != null) {
            ldError.postValue(true)
            if (!response.error.message.isNullOrBlank()) {
                showToast(response.error.message)
            }
            return
        }
        response.data?.let {
            photoData.clear()
            for (photo in it){
                photo.urls?.let { it1 -> photoData.add(it1) }
            }
            if (newData) {
                ldSuccess.postValue(photoData)
            } else {
                ldSuccessAddData.postValue(photoData)
            }
        }
    }
}