package com.rozetkatesttask.ui.fullphoto

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import com.rozetkatesttask.R
import com.rozetkatesttask.api.models.listphoto.PhotosResponse
import com.rozetkatesttask.api.models.listphoto.Urls
import com.rozetkatesttask.databinding.ActivityFullPhotoBinding
import com.rozetkatesttask.mvvmcore.BaseActivity
import com.squareup.picasso.Picasso
import java.io.File
import java.util.*


private const val ACTIVITY_LAYOUT_ID = R.layout.activity_full_photo
private const val EXTRA_PHOTO_FULL = "PHT_DATA_FULL"

class FullPhotoActivity : BaseActivity<FullPhotoViewModel, ActivityFullPhotoBinding>() {
    companion object {
        fun newIntent(context: Context?, photosResponse: Urls) =
            Intent(
                context,
                FullPhotoActivity::class.java
            ).putExtra(EXTRA_PHOTO_FULL, photosResponse.full)
    }

    lateinit var link: String

    override val layoutId: Int = ACTIVITY_LAYOUT_ID

    override fun createViewMode() = provideViewModel { FullPhotoViewModel(it) }

    override fun setDataToBinding() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        link = intent.getStringExtra(EXTRA_PHOTO_FULL)!!
        initView()
        setListeners()
    }
    private fun initView() {
        Picasso.with(this)
            .load(link)
            .placeholder(R.drawable.ic_image_placeholder)
            .error(R.drawable.ic_no_image)
            .into(binding.fullImage);
    }

    private fun setListeners() {
        binding.btnDownload.setOnClickListener {
            downloadImageNew("RZT_test_task" + Date(), link)
        }

        binding.btnGoBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun downloadImageNew(filename: String, downloadUrlOfImage: String) {
        try {
            val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            val downloadUri: Uri = Uri.parse(downloadUrlOfImage)
            val request = DownloadManager.Request(downloadUri)
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(filename)
                .setMimeType("image/jpeg") // Your file type. You can use this code to download other file types also.
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_PICTURES,
                    File.separator.toString() + filename + ".jpg"
                )
            dm.enqueue(request)
            Toast.makeText(this, resources.getString(R.string.downloading_image), Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(this, resources.getString(R.string.fail_downloading_image), Toast.LENGTH_SHORT).show()
        }
    }
}
