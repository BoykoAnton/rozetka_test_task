package com.rozetkatesttask.mvvmcore

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rozetkatesttask.ui.widgets.ProcessProgressDialog
import com.rozetkatesttask.utils.UIUtils

abstract class BaseFragment<VM : BaseViewModel, B : ViewDataBinding> : Fragment() {

    protected abstract val layoutId: Int
    protected val viewModel: VM by lazy { createViewMode() }
    protected lateinit var binding: B

    private val progressDialog by lazy { ProcessProgressDialog(activity) }

    abstract fun createViewMode(): VM
    abstract fun setDataToBinding()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(layoutInflater, layoutId, container, false)
        setDataToBinding()
        setObservers()
        lifecycle.addObserver(viewModel)

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        viewModel.onHandleActivityResult(requestCode, resultCode, data)
    }

    private fun setObservers() {
        viewModel.ldErrorToast.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.ldProgressDialog.observe(viewLifecycleOwner, Observer { isShow ->
            if (isShow)
                UIUtils.showProgressDialog(progressDialog, false)
            else
                UIUtils.hideProgressDialog(progressDialog)
        })

        viewModel.ldStartActivityForResult.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            val intent = it.first(activity)
            val requestCode = it.second

            startActivityForResult(intent, requestCode)
            viewModel.ldStartActivityForResult.postValue(null)
        })

        viewModel.ldStartActivity.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            startActivity(it(activity))

            viewModel.ldStartActivity.postValue(null)
        })
    }

    protected inline fun <reified VMP : VM> provideViewModel(noinline instance: (application: Application) -> VMP): VMP {
        return ViewModelProviders.of(this, ViewModelFactory(activity?.application!!, instance))
            .get(VMP::class.java)
    }
}