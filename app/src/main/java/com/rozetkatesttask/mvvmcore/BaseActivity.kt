package com.rozetkatesttask.mvvmcore

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rozetkatesttask.ui.widgets.ProcessProgressDialog
import com.rozetkatesttask.utils.UIUtils

abstract class BaseActivity<VM : BaseViewModel, B : ViewDataBinding> : AppCompatActivity() {

    protected abstract val layoutId: Int
    protected val viewModel: VM by lazy { createViewMode() }
    protected lateinit var binding: B

    private val progressDialog by lazy { ProcessProgressDialog(this) }

    abstract fun createViewMode(): VM
    abstract fun setDataToBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, layoutId)
        setDataToBinding()
        setObservers()

        lifecycle.addObserver(viewModel)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        viewModel.onHandleActivityResult(requestCode, resultCode, data)
    }

    private fun setObservers() {
        viewModel.ldErrorToast.observe(this, Observer {
            it ?: return@Observer
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.ldProgressDialog.observe(this, Observer { isShow ->
            if (isShow)
                UIUtils.showProgressDialog(progressDialog, false)
            else
                UIUtils.hideProgressDialog(progressDialog)
        })

        viewModel.ldStartActivityForResult.observe(this, Observer {
            it ?: return@Observer

            val intent = it.first(this)
            val requestCode = it.second

            startActivityForResult(intent, requestCode)
            viewModel.ldStartActivityForResult.postValue(null)
        })

        viewModel.ldStartActivity.observe(this, Observer {
            it ?: return@Observer

            startActivity(it(this))
            viewModel.ldStartActivity.postValue(null)
        })
    }

    protected inline fun <reified VMP : VM> provideViewModel(noinline instance: (application: Application) -> VMP): VMP {
        return ViewModelProviders.of(this, ViewModelFactory(application, instance))
            .get(VMP::class.java)
    }

}