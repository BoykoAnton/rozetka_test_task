package com.rozetkatesttask.mvvmcore

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable
import com.rozetkatesttask.BuildConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

open class BaseViewModel(application: Application) : AndroidViewModel(application),
    LifecycleObserver {
    val ldProgressPagination = MutableLiveData<Boolean>()
    val ldProgressDialog = MutableLiveData<Boolean>()

    val ldErrorToast = MutableLiveData<String?>()

    val ldStartActivityForResult = MutableLiveData<Pair<(context: Context?) -> Intent, Int>?>()
    val ldStartActivity = MutableLiveData<((context: Context?) -> Intent)?>()

    protected val parentJob = Job()
    protected val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.IO
    protected val scope = CoroutineScope(coroutineContext)


    protected val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    open fun onHandleActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    protected fun showProgress(
        progressDialog: Boolean = false,
        progressPagination: Boolean = false
    ) {
        ldProgressDialog.postValue(progressDialog)
        ldProgressPagination.postValue(progressPagination)
    }

    protected fun hideProgress() {
        ldProgressDialog.postValue(false)
        ldProgressPagination.postValue(false)
    }

    protected open fun onError(throwable: Throwable) {
        if (BuildConfig.DEBUG) throwable.printStackTrace()
        hideProgress()
    }

    protected fun showToast(error: String?) {
        ldErrorToast.postValue(error)
    }
}